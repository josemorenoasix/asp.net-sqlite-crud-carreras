using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrudCarreras.Data;
using CrudCarreras.Models;
using Microsoft.AspNetCore.Http;

namespace CrudCarreras.Controllers
{
    public class CarrerasController : Controller
    {
        private readonly CrudCarrerasContext _context;

        public CarrerasController(CrudCarrerasContext context)
        {
            _context = context;
        }

        // GET: Carreras
        public async Task<IActionResult> Index()
        {
            var crudCarrerasContext = _context.Carreras.Include(c => c.Participantes);
            return View(await crudCarrerasContext.ToListAsync());
        }

        // GET: Carreras/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var carreraModel = await _context.Carreras
                .Include(c => c.Participantes)
                .ThenInclude(p => p.Dorsal)
                .FirstOrDefaultAsync(m => m.CarreraId == id);
            if (carreraModel == null)
            {
                return NotFound();
            }

            return View(carreraModel);
        }

        // GET: Carreras/Create
        public IActionResult Create()
        {
            return View();
        }
        
        

        // POST: Carreras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CarreraId,Distancia,Descripcion,Fecha")] CarreraModel carreraModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(carreraModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(carreraModel);
        }

        // GET: Carreras/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var carreraModel = await _context.Carreras.FindAsync(id);
            if (carreraModel == null)
            {
                return NotFound();
            }
            return View(carreraModel);
        }

        // POST: Carreras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CarreraId,Distancia,Descripcion,Fecha")] CarreraModel carreraModel)
        {
            if (id != carreraModel.CarreraId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(carreraModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CarreraModelExists(carreraModel.CarreraId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(carreraModel);
        }

        // GET: Carreras/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var carreraModel = await _context.Carreras
                .FirstOrDefaultAsync(m => m.CarreraId == id);
            if (carreraModel == null)
            {
                return NotFound();
            }

            return View(carreraModel);
        }

        // POST: Carreras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var carreraModel = await _context.Carreras.FindAsync(id);
            _context.Carreras.Remove(carreraModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CarreraModelExists(int id)
        {
            return _context.Carreras.Any(e => e.CarreraId == id);
        }

        // POST: Carreras/UploadCodigos/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadCodigos(int? id, IFormFile codigosFile)
        {
            
            if (id == null)
            {
                return NotFound();
            }
            
            var carreraModel = _context.Carreras
                .Include(c => c.Dorsales)
                .First(c => c.CarreraId == id);
            if (carreraModel == null)
            {
                return NotFound();
            }
            
            
            if (codigosFile.Length > 0)
            {
                
                var result = new StringBuilder();
                using (var reader = new StreamReader(codigosFile.OpenReadStream()))
                {
                    while (reader.Peek() >= 0)
                        result.AppendLine(await reader.ReadLineAsync()); 
                }
                
                Regex regex = new Regex(@"^([a-fA-F\d]{16}) {4}(0*\d{1,"+carreraModel.Dorsales.Count.ToString().Length+"})$", RegexOptions.Multiline);
                MatchCollection matchCollection = regex.Matches(result.ToString());
                foreach (Match m in matchCollection)
                {
                    GroupCollection g = m.Groups;
                    string codigo = g[1].Value;
                    try
                    {
                        int dorsal = Int32.Parse(g[2].Value);
                        
                        DorsalModel dorsalModel = _context.Dorsales
                            .FirstOrDefault(d => d.Numero == dorsal);

                        if (dorsalModel == null)
                        {
                            Console.WriteLine("No existe participante asociado a la siguiente linea: " + g[0].Value);
                        }
                        else
                        {
                            dorsalModel.Codigo = codigo;
                            _context.Update(dorsalModel);
                            _context.SaveChanges();
                        }
                        
                       
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Invalid dorsal number for code: " + codigo);
                        throw;
                    }
                }
             
            }
            
            return RedirectToAction(nameof(Details),new {id=id})
                .WithInfo("Actualización finalizada", "Los dorsales han sido asignados");
        }
        
        // POST: Carreras/UploadTiempos/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadTiempos(int? id, IFormFile tiemposFile)
        {
            
            if (id == null)
            {
                return NotFound();
            }
            
            var carreraModel = _context.Carreras
                .Include(c => c.Dorsales)
                .First(c => c.CarreraId == id);
            if (carreraModel == null)
            {
                return NotFound();
            }
            
            
            if (tiemposFile.Length > 0)
            {
                
                var result = new StringBuilder();
                using (var reader = new StreamReader(tiemposFile.OpenReadStream()))
                {
                    while (reader.Peek() >= 0)
                        result.AppendLine(await reader.ReadLineAsync()); 
                }
                
                Regex regex = new Regex(@"^([a-fA-F\d]{16}) {4}(\d{8})$", RegexOptions.Multiline);
                IOrderedEnumerable<Match> matchCollection = regex.Matches(result.ToString()).OrderBy(m => m.Groups[2].Value);
                int posicion = 1;
                foreach (Match m in matchCollection)
                {
                    GroupCollection g = m.Groups;
                    string codigo = g[1].Value;
                    
                    try
                    {
                        DateTime horaLlegada = DateTime.ParseExact(g[2].Value ,"HHmmssff", CultureInfo.InvariantCulture);
                        
                        var horaInicio = carreraModel.Fecha;
                        TimeSpan tiempo = new TimeSpan(0,
                            horaLlegada.Hour - horaInicio.Hour,
                            horaLlegada.Minute - horaInicio.Minute,
                            horaLlegada.Second - horaInicio.Second,
                            horaLlegada.Millisecond  - horaInicio.Millisecond);
                        
                        ParticipanteModel participanteModel = _context.Participantes
                            .Include(p => p.Dorsal)
                            .FirstOrDefault(p => p.Dorsal.Codigo == codigo);
                        

                        if (participanteModel == null)
                        {
                            Console.WriteLine("No existe participante asociado a la siguiente linea: " + g[0].Value);
                        }
                        else
                        {
                            participanteModel.Tiempo = tiempo.TotalSeconds;
                            participanteModel.Posicion = posicion++;
                            _context.Update(participanteModel);
                            _context.SaveChanges();
                        }
                        
                       
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Invalid dorsal number for code: " + codigo);
                        throw;
                    }
                }
             
            }
            
            return RedirectToAction(nameof(Details),new {id=id})
                .WithInfo("Actualización finalizada", "Los dorsales han sido asignados");
        }
        
        
    }
}
