using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CrudCarreras.Data;
using CrudCarreras.Models;

namespace CrudCarreras.Controllers
{
    public class ParticipantesController : Controller
    {
        private readonly CrudCarrerasContext _context;

        public ParticipantesController(CrudCarrerasContext context)
        {
            _context = context;
        }

        // GET: Participantes
        public async Task<IActionResult> Index()
        {
            var crudCarrerasContext = _context.Participantes
                .Include(p => p.Carrera)
                .Include(p => p.Dorsal);
            
            return View(await crudCarrerasContext.ToListAsync());
        }

        // GET: Participantes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participanteModel = await _context.Participantes
                .Include(p => p.Carrera)
                .Include(p => p.Dorsal)
                .FirstOrDefaultAsync(m => m.ParticipanteId == id);
            if (participanteModel == null)
            {
                return NotFound();
            }

            return View(participanteModel);
        }

        // GET: Participantes/Create
        public IActionResult Create()
        {
            ViewData["CarreraId"] = new SelectList(_context.Carreras, "CarreraId", "Descripcion");
            return View();
        }

        // POST: Participantes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ParticipanteId,Nombre,Apellidos,DNI,Club,CarreraId")] ParticipanteModel participanteModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(participanteModel);
                _context.SaveChanges();
                

                ICollection<DorsalModel> dorsalesCarrera = _context.Carreras
                    .Include(c => c.Dorsales)
                    .First(c => c.CarreraId == participanteModel.CarreraId).Dorsales;
                int lastDorsalNumber = dorsalesCarrera.Count == 0 ? 0 : dorsalesCarrera.OrderBy(d => d.Numero).Last().Numero;
                DorsalModel nextDorsal = new DorsalModel();
                nextDorsal.Numero = lastDorsalNumber + 1;
                nextDorsal.CarreraId = participanteModel.CarreraId;
                nextDorsal.ParticipanteId = participanteModel.ParticipanteId;
                _context.Add(nextDorsal);
                _context.SaveChanges();
                
                participanteModel.DorsalId = nextDorsal.DorsalId;
                _context.Update(participanteModel);
                
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CarreraId"] = new SelectList(_context.Carreras, "CarreraId", "Descripcion", participanteModel.CarreraId);
            return View(participanteModel);
        }

        // GET: Participantes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participanteModel = await _context.Participantes
                .Include(p => p.Dorsal)
                .FirstOrDefaultAsync(p => p.ParticipanteId == id);
            if (participanteModel == null)
            {
                return NotFound();
            }
            ViewData["CarreraId"] = new SelectList(_context.Carreras, "CarreraId", "Descripcion", participanteModel.CarreraId);
            return View(participanteModel);
        }

        // POST: Participantes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ParticipanteId,Nombre,Apellidos,DNI,Club")] ParticipanteModel participanteModel)
        {
            if (id != participanteModel.ParticipanteId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Participantes.Attach(participanteModel);
                    _context.Entry(participanteModel).Property(p => p.Nombre).IsModified = true;
                    _context.Entry(participanteModel).Property(p => p.Apellidos).IsModified = true;
                    _context.Entry(participanteModel).Property(p => p.DNI).IsModified = true;
                    _context.Entry(participanteModel).Property(p => p.Club).IsModified = true;
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ParticipanteModelExists(participanteModel.ParticipanteId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CarreraId"] = new SelectList(_context.Carreras, "CarreraId", "Descripcion", participanteModel.CarreraId);
            return View(participanteModel);
        }

        // GET: Participantes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participanteModel = await _context.Participantes
                .Include(p => p.Carrera)
                .Include(p => p.Dorsal)
                .FirstOrDefaultAsync(m => m.ParticipanteId == id);
            if (participanteModel == null)
            {
                return NotFound();
            }

            return View(participanteModel);
        }

        // POST: Participantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var participanteModel = await _context.Participantes.FindAsync(id);
            _context.Participantes.Remove(participanteModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ParticipanteModelExists(int id)
        {
            return _context.Participantes.Any(e => e.ParticipanteId == id);
        }
    }
}
