﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CrudCarreras.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Carreras",
                columns: table => new
                {
                    CarreraId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Distancia = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(nullable: false),
                    Fecha = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carreras", x => x.CarreraId);
                });

            migrationBuilder.CreateTable(
                name: "Dorsales",
                columns: table => new
                {
                    DorsalId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Numero = table.Column<int>(nullable: false),
                    Codigo = table.Column<string>(nullable: true),
                    ParticipanteId = table.Column<int>(nullable: false),
                    CarreraId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dorsales", x => x.DorsalId);
                    table.ForeignKey(
                        name: "FK_Dorsales_Carreras_CarreraId",
                        column: x => x.CarreraId,
                        principalTable: "Carreras",
                        principalColumn: "CarreraId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Participantes",
                columns: table => new
                {
                    ParticipanteId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nombre = table.Column<string>(maxLength: 20, nullable: false),
                    Apellidos = table.Column<string>(maxLength: 50, nullable: false),
                    DNI = table.Column<string>(maxLength: 12, nullable: false),
                    Club = table.Column<string>(nullable: true),
                    Tiempo = table.Column<int>(nullable: true),
                    Posicion = table.Column<int>(nullable: true),
                    DorsalId = table.Column<int>(nullable: true),
                    CarreraId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participantes", x => x.ParticipanteId);
                    table.ForeignKey(
                        name: "FK_Participantes_Carreras_CarreraId",
                        column: x => x.CarreraId,
                        principalTable: "Carreras",
                        principalColumn: "CarreraId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Participantes_Dorsales_DorsalId",
                        column: x => x.DorsalId,
                        principalTable: "Dorsales",
                        principalColumn: "DorsalId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dorsales_CarreraId",
                table: "Dorsales",
                column: "CarreraId");

            migrationBuilder.CreateIndex(
                name: "IX_Dorsales_ParticipanteId",
                table: "Dorsales",
                column: "ParticipanteId");

            migrationBuilder.CreateIndex(
                name: "IX_Participantes_CarreraId",
                table: "Participantes",
                column: "CarreraId");

            migrationBuilder.CreateIndex(
                name: "IX_Participantes_DorsalId",
                table: "Participantes",
                column: "DorsalId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dorsales_Participantes_ParticipanteId",
                table: "Dorsales",
                column: "ParticipanteId",
                principalTable: "Participantes",
                principalColumn: "ParticipanteId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dorsales_Carreras_CarreraId",
                table: "Dorsales");

            migrationBuilder.DropForeignKey(
                name: "FK_Participantes_Carreras_CarreraId",
                table: "Participantes");

            migrationBuilder.DropForeignKey(
                name: "FK_Dorsales_Participantes_ParticipanteId",
                table: "Dorsales");

            migrationBuilder.DropTable(
                name: "Carreras");

            migrationBuilder.DropTable(
                name: "Participantes");

            migrationBuilder.DropTable(
                name: "Dorsales");
        }
    }
}
