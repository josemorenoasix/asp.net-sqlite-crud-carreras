using System;
using System.Collections.Generic;
using CrudCarreras.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace CrudCarreras.Models
{
    public class CrudCarrerasInitializer

    {
        public static void Initialize(IServiceProvider serviceProvider)
        {

            using (var context = new CrudCarrerasContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<CrudCarrerasContext>>()))
            {

                if (!context.Carreras.Any())
                {
                    var carreras = new List<CarreraModel>
                    {
                        new CarreraModel
                        {
                            CarreraId = 1,
                            Distancia = 10000,
                            Descripcion = "XI Volta a la Safor",
                            Fecha = DateTime.Parse("2019-2-12 16:30"),
                        },
                        new CarreraModel
                        {
                            CarreraId = 2,
                            Distancia = 20000,
                            Descripcion = "IV Mitja Marató a la Marjal Pego-Oliva",
                            Fecha = DateTime.Parse("2019-3-20 10:30"),
                        }
                    };
                    context.Carreras.AddRange(carreras);
                    context.SaveChanges();
                }

                if (!context.Participantes.Any())
                {
                    var participantes = new List<ParticipanteModel>();
                    var dorsales = new List<DorsalModel>();
                    for (var i = 1; i < 140; i++)
                    {
                        var dump = i.ToString();
                        var participante = new ParticipanteModel
                        {
                            ParticipanteId = i,
                            CarreraId = 1,
                            Nombre = "Nombre-" + dump,
                            Apellidos = "Apellidos-" + dump,
                            DNI = "AAAAAAAA-" + dump,
                            Club = "Club-" + dump
                        };
                        context.Add(participante);
                        context.SaveChanges();

                        var dorsal = new DorsalModel
                        {
                            DorsalId = i,
                            Numero = i,
                            ParticipanteId = i,
                            CarreraId = 1,
                        };
                        context.Dorsales.Add(dorsal);
                        context.SaveChanges();

                        participante.DorsalId = i;
                        context.Participantes.Attach(participante);
                        context.Entry(participante).Property(p => p.DorsalId).IsModified = true;
                        context.SaveChanges();
                    }
                }

            }

        }

    }
}
