
using Microsoft.EntityFrameworkCore;
using CrudCarreras.Models;

namespace CrudCarreras.Data
{
    public class CrudCarrerasContext : DbContext
    {
        public CrudCarrerasContext (DbContextOptions<CrudCarrerasContext> options)
            : base(options)
        {
        }

        public DbSet<CarreraModel> Carreras { get; set; }
        public DbSet<ParticipanteModel> Participantes { get; set; }
        
        public DbSet<DorsalModel> Dorsales { get; set; }
        
        
        

    }
}