using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrudCarreras.Models
{
    public class DorsalModel
    {
    
        [Key]
        public int DorsalId { get; set; }
        
        [Required]
        public int Numero { get; set; }
        
        public string Codigo { get; set; }
        
        [Required]
        public int ParticipanteId { get; set; }
        
        [Required]
        public int CarreraId { get; set; }
        
        [ForeignKey("ParticipanteId")]
        public virtual ParticipanteModel Participante { get; set; }
        
        [ForeignKey("CarreraId")]
        public virtual CarreraModel Carrera { get; set; }
        
    }
}