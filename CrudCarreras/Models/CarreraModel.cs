using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CrudCarreras.Models
{
    public class CarreraModel
    {
    
        [Key]
        public int CarreraId { get; set; }
        
        [Required]
        public int Distancia { get; set; }
        
        [Required]
        public string Descripcion { get; set; }
        
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
       
        public virtual ICollection<ParticipanteModel> Participantes { get; set; }
        
        public virtual ICollection<DorsalModel> Dorsales { get; set; }
        
    }
}