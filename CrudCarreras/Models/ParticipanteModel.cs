using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrudCarreras.Models
{
    public class ParticipanteModel
    {
        [Key]
        public int ParticipanteId { get; set; }
        
        [Required]
        [StringLength(20, ErrorMessage = "Nombre cannot be longer than 20 characters.")]
        public string Nombre { get; set; }
        
        [Required]
        [StringLength(50, ErrorMessage = "Apellidos cannot be longer than 50 characters.")]
        public string Apellidos { get; set; }
        
        [Required]
        [StringLength(12, MinimumLength=8, ErrorMessage = "DNI cannot be shorter than 8 characters and longer than 12.")]
        public string DNI { get; set; }
        
        public string Club { get; set; }
        
        [DataType(DataType.Duration)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public double? Tiempo { get; set; }
        
        public int? Posicion { get; set; }
        
        public int? DorsalId { get; set; }
        
        [ForeignKey("DorsalId")]
        public virtual DorsalModel Dorsal { get; set;}
        
        [Required]
        public int CarreraId { get; set; }
        
        [ForeignKey("CarreraId")]
        public virtual CarreraModel Carrera { get; set;}

        public string MarcaPersonal
        {
            get
            {
                var tiempo = Tiempo;
                if (tiempo == null) return "";
                return TimeSpan.FromSeconds(tiempo.Value).ToString("g");
            }
        }
        
    }
    
    
}